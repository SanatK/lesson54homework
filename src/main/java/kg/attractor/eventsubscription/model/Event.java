package kg.attractor.eventsubscription.model;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Document(collection="events")
public class Event {
    @Id
    @Builder.Default
    private String id = UUID.randomUUID().toString();
    private LocalDateTime eventDate;
    @Indexed
    private String name;
    private String description;
}
