package kg.attractor.eventsubscription.dto;
import kg.attractor.eventsubscription.model.EventSubscription;
import lombok.*;
import java.time.LocalDateTime;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EventSubscriptionDTO {
    public static EventSubscriptionDTO from(EventSubscription eventSubscription){
        return builder()
                .id(eventSubscription.getId())
                .eventId(eventSubscription.getEventId())
                .email(eventSubscription.getEmail())
                .registrationTime(eventSubscription.getRegistrationTime())
                .build();
        }
    private String id;
    private String eventId;
    private String email;
    private LocalDateTime registrationTime;

}
