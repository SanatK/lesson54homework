package kg.attractor.eventsubscription.dto;
import kg.attractor.eventsubscription.model.Event;
import lombok.*;

import java.time.LocalDateTime;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EventDTO {
    public static EventDTO from(Event event){
        return builder()
                .id(event.getId())
                .eventDate(event.getEventDate())
                .name(event.getName())
                .description(event.getDescription())
                .build();
    }
    private String id;
    private LocalDateTime eventDate;
    private String name;
    private String description;
}
