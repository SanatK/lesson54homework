package kg.attractor.eventsubscription.controller;
import kg.attractor.eventsubscription.dto.EventDTO;
import kg.attractor.eventsubscription.dto.EventSubscriptionDTO;
import kg.attractor.eventsubscription.model.EventSubscription;
import kg.attractor.eventsubscription.service.EventSubscriptionService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/subscriptions")
public class EventSubscriptionController {
    private final EventSubscriptionService eventSubscriptionService;
    public EventSubscriptionController(EventSubscriptionService eventSubscriptionService){
        this.eventSubscriptionService = eventSubscriptionService;
    }
    //этот метод принимает строку текстового вида, такого формата -> sanat@gmail.com
    @PostMapping
    public List<EventDTO> getUserEvents(@RequestBody String email){
        return eventSubscriptionService.getUserSubscriptions(email);
    }

    @PostMapping("/subscribe")
    public String subscribeOnEvent(@RequestBody EventSubscriptionDTO eventSubscriptionData){
        return eventSubscriptionService.addSubscription(eventSubscriptionData);
    }
    @DeleteMapping
    public String deleteSubscription(@RequestBody EventSubscriptionDTO eventSubscriptionDTO){
        return eventSubscriptionService.deleteSubscription(eventSubscriptionDTO);
    }
}
