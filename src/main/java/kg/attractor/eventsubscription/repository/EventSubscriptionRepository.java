package kg.attractor.eventsubscription.repository;
import kg.attractor.eventsubscription.model.EventSubscription;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EventSubscriptionRepository extends CrudRepository<EventSubscription, String> {
  List<EventSubscription> findAllByEmail(String email);
  EventSubscription findByEmail(String email);
}
