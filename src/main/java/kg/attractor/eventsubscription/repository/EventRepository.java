package kg.attractor.eventsubscription.repository;
import kg.attractor.eventsubscription.model.Event;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface EventRepository extends PagingAndSortingRepository<Event, String> {
    @Query("{'id':?0}")
    LocalDateTime getEventTimeById(String eventId);


}
