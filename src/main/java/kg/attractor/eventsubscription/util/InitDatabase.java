package kg.attractor.eventsubscription.util;

import kg.attractor.eventsubscription.model.Event;
import kg.attractor.eventsubscription.repository.EventRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Configuration
public class InitDatabase {
    @Bean
    CommandLineRunner init(EventRepository eventRepository) {
        return (args) -> {
            eventRepository.deleteAll();
            eventRepository.saveAll(createEvents());
        };
    }


    public List<Event> createEvents() {
        List<Event> events = new ArrayList<>();
        events.add(new Event("001", getRandomTime(), "FootballMatch", "Real-Madrid - Barcelona"));
        events.add(new Event("002", getRandomTime(), "Party", "party in Club"));
        events.add(new Event("003", getRandomTime(), "Concert", "concert in Hall"));
        events.add(new Event("004", getRandomTime(), "Presentation", "new TED presentation"));
        return events;
    }


    public LocalDateTime getRandomTime(){
        Random r = new Random();
        int ran = r.nextInt(100);
        if(ran>50)
            return LocalDateTime.now().minusHours(ran);
            return LocalDateTime.now().plusHours(ran);
    }
}