package kg.attractor.eventsubscription.service;

import kg.attractor.eventsubscription.dto.EventDTO;
import kg.attractor.eventsubscription.dto.EventSubscriptionDTO;
import kg.attractor.eventsubscription.model.Event;
import kg.attractor.eventsubscription.model.EventSubscription;
import kg.attractor.eventsubscription.repository.EventRepository;
import kg.attractor.eventsubscription.repository.EventSubscriptionRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EventSubscriptionService {
   private final EventSubscriptionRepository eventSubscriptionRepository;
   private final EventRepository eventRepository;
    public EventSubscriptionService(EventSubscriptionRepository eventSubscriptionRepository, EventRepository eventRepository){
        this.eventSubscriptionRepository = eventSubscriptionRepository;
        this.eventRepository = eventRepository;
    }
    public String addSubscription(EventSubscriptionDTO eventSubscriptionData){
        if(eventRepository.findById(eventSubscriptionData.getEventId()).get().getEventDate().isAfter(LocalDateTime.now())) {
            var subscription = EventSubscription.builder()
                    .id(eventSubscriptionData.getId())
                    .eventId(eventSubscriptionData.getEventId())
                    .email(eventSubscriptionData.getEmail())
                    .registrationTime(LocalDateTime.now())
                    .build();
            eventSubscriptionRepository.save(subscription);
            return String.format("subscription id: %s, %s", subscription.getId(), "You successfully subscribed on event!"); }
        return "Unfortunately this event has passed";
    }
    public List<EventDTO> getUserSubscriptions(String email){
        List<EventSubscription> userSubscriptions = new ArrayList<>();
        userSubscriptions.addAll(eventSubscriptionRepository.findAllByEmail(email));
        List<Event> userEvents = new ArrayList<>();
        for(EventSubscription e: userSubscriptions){
            userEvents.add(eventRepository.findById(e.getEventId()).get());
        }
        List<EventDTO> userEventsDTO = new ArrayList<>();
        for(Event e: userEvents){
            userEventsDTO.add(EventDTO.from(e));
        }
        return userEventsDTO;
    }
    public String deleteSubscription(EventSubscriptionDTO eventSubscriptionDTO){
        List<EventSubscription> userSubscriptions = new ArrayList<>();
        userSubscriptions.addAll(eventSubscriptionRepository.findAllByEmail(eventSubscriptionDTO.getEmail()));
        System.out.println(eventSubscriptionRepository.findAllByEmail(eventSubscriptionDTO.getEmail()));
        for(EventSubscription e: userSubscriptions){
            if(e.getEventId().equals(eventSubscriptionDTO.getEventId())){
                eventSubscriptionRepository.delete(e);
                return "Your subscription was successfully deleted";
            }
        }
        return "Your subscription wasn't deleted: Check event ID";
    }
}
