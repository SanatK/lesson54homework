package kg.attractor.eventsubscription.service;

import kg.attractor.eventsubscription.dto.EventDTO;
import kg.attractor.eventsubscription.model.Event;
import kg.attractor.eventsubscription.repository.EventRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EventService {
    private final EventRepository eventRepository;
    public EventService(EventRepository eventRepository){
        this.eventRepository = eventRepository;
    }
    public Slice<EventDTO> findEvents(Pageable pageable) {
        var slice = eventRepository.findAll(pageable);
        return slice.map(EventDTO::from);
    }
}
